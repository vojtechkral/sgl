#ifndef SGL_CONTEXT
#define SGL_CONTEXT


#include <cstring>
//DEBUG
#include <iostream>	
using namespace std;

struct vec2 { float x,y; vec2(float x, float y): x(x),y(y){}; vec2(){}; };
struct vec3 { float x,y,z; vec3(float x, float y,float z): x(x),y(y),z(z){}; vec3(){}; };
struct vec4 { float x,y,z,w; vec4(float x, float y,float z,float w): x(x),y(y),z(z),w(w){}; vec4(){};};

struct Buffer {
	//TODO replace float with some math lib vec3
	float * data;

	int width;
	int height;
	int components;

	Buffer(const int w, const int h, const int components = 3) : width(w), height(h), components(components) {
		data = new float[width*height*components];		
	}

	inline void clear(const vec4 &clearColor){
		for(int i=0; i < width*height*components; i+=3){	
			memcpy(data + i, &clearColor, sizeof(float)*3);					
		}
	}

	inline void setPixel(const vec3 &color,int x, int y){
		memcpy(data + y*width + x, &color, sizeof(float)*3);
	}

};


class Context{

public:

	Buffer * buffer;
	vec4 clearColor;
	vec3 color;
	sglEAreaMode areaMode;
	float pointSize;
	bool depthTest;

	bool begin;

	Context(const int width, const int height){		
		
		buffer = new Buffer(width,height);		
		clearColor = vec4(0,0,0,1);
		begin = false;
		color = vec3(1,1,1);
		areaMode = SGL_FILL;
		pointSize = 1.0f;
		depthTest = false;

	}

	~Context(){
		delete buffer;
	}



};



#endif
