#ifndef SGL_MATH
#define SGL_MATH

#include <cmath>
#include <cstdlib>
#ifdef _DEBUG
#include <iostream>
#endif

#include "utils.h"

#define M_PI 3.14159265359
#define EPSILON 1e-6f


/*
	2xfloat vector
*/
struct vec2 {
	float x,y;
	vec2(float x, float y): x(x),y(y){};
	vec2(){
		memset(this,0x0,2*sizeof(float));
	};

	float operator [](int i){
		return *(((float *)this) + i);
	}


#ifdef _DEBUG
	friend std::ostream & operator << (std::ostream& os, const vec2& v){
		os << "(" << v.x <<", " << v.y << ")"<< std::endl;
		return os;
	}
#endif
};

/*
	2xint vector
*/
struct ivec2 {
	int x,y;
	ivec2(int x, int y): x(x),y(y){};
	ivec2(){
		memset(this,0x0,2*sizeof(int));
	};

	float operator [](int i){
		return *(((float *)this) + i);
	}

	bool operator==(const ivec2& other) const {
		return x == other.x && y == other.y;
	}

#ifdef _DEBUG
	friend std::ostream & operator << (std::ostream& os, const ivec2& v){
		os << "(" << v.x <<", " << v.y << ")"<< std::endl;
		return os;
	}
#endif
};

/*
	3xfloat vector
*/
struct vec3 {
	float x,y,z;
	vec3(float x, float y,float z): x(x),y(y),z(z){};
	vec3(){
		memset(this,0x0,3*sizeof(float));
	};
	vec3(const vec3& a): x(a.x), y(a.y), z(a.z) {}

	vec3 operator * (float a) const{
		return vec3(a*x,a*y,a*z);
	}

	vec3 operator * (vec3 a) const{
		return vec3(a.x*x,a.y*y,a.z*z);
	}

	vec3 operator / (float a) const {
		return vec3(x / a, y / a, z / a);
	}

	bool operator==(const vec3& a) const {
		return x == a.x && y == a.y && z == a.z;
	}

	bool operator!=(const vec3& a) const {
		return !operator==(a);
	}

	float operator [](int i){
		return *(((float *)this) + i);
	}

	float operator [](const int i) const{
		return *(((float *)this) + i);
	}

	vec3 abs() const {
		return vec3(fabs(x), fabs(y), fabs(z));
	}

	float max() const {
		return ::max(::max(x, y), z);
	}

	void cap(float at) {
		x = min(x, at);
		y = min(y, at);
		z = min(z, at);
	}

#ifdef _DEBUG
	friend std::ostream & operator << (std::ostream& os, const vec3& v){
		os << "(" << v.x <<", " << v.y << ", " << v.z << ")"<< std::endl;
		return os;
	}
#endif
};

/*
	4xfloat vector
*/
struct vec4 {
	float x,y,z,w;
	vec4(float x, float y,float z,float w): x(x),y(y),z(z),w(w){};
	vec4(){
		memset(this,0x0,4*sizeof(float));
	};

	vec4 operator * (float a){
		return vec4(a*x,a*y,a*z,a*w);
	}

	float operator [](int i){
		return *(((float *)this) + i);
	}

	float operator [](const int i) const{
		return *(((float *)this) + i);
	}

#ifdef _DEBUG
	friend std::ostream & operator << (std::ostream& os, const vec4& v){
		os << "(" << v.x <<", " << v.y << ", " << v.z << ", " << v.w << ")"<< std::endl;
		return os;
	}
#endif

};

inline vec4 operator - (vec4 lhs, const vec4& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
	lhs.w -= rhs.w;
	return  lhs;
}
inline vec3 operator - (vec3 lhs, const vec3& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
	return  lhs;
}

inline vec3 operator + (vec3 lhs, const vec3& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;
	return  lhs;
}

inline vec2 operator + (vec2 lhs, vec2& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	return  lhs;
}

inline ivec2 operator + (ivec2 lhs, ivec2& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	return  lhs;
}

inline vec2 operator - (vec2 lhs, vec2& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	return  lhs;
}

inline ivec2 operator - (ivec2 lhs, ivec2& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	return  lhs;
}


template <typename T>
inline float dst(const T &a, const T &b){
	float sum = 0.0f;
	for(int i=0; i < sizeof(a)/sizeof(float); i++)
		sum += (a[i]-b[i])*(a[i]-b[i]);

	return sqrt(sum);
}

template <typename T>
inline float dst_2(const T &a, const T &b){
	float sum = 0.0f;
	for(int i=0; i < 2; i++)
		sum += (a[i]-b[i])*(a[i]-b[i]);

	return sqrt(sum);
}

template <typename T>
inline float length_2(const T &a){
	return sqrt(a.x*a.x +a.y*a.y);
}

/*
inline float dst(const vec2 &a,const vec2 &b){
	return sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}
inline float dst(const ivec2 &a,const ivec2 &b){
	return sqrt(float((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y)));
}

inline float dst_2(const vec4 &a,const vec4 &b){
	return sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}
*/
inline float dot_2(const vec4 &a,const vec4 &b){
	return a.x*b.x + b.x*b.y;
}

inline float dot(const vec2 &a,const vec2 &b){
	return a.x*b.x + b.x*b.y;
}

inline float dot(const ivec2 &a,const ivec2 &b){
	return a.x*b.x + b.x*b.y;
}


inline float length(const vec2 &a){
	return sqrt(a.x*a.x +a.y*a.y);
}

inline float length(const ivec2 &a){
	return sqrt(float(a.x*a.x +a.y*a.y));
}

inline float length(const vec3 &a){
	return sqrt(a.x*a.x +a.y*a.y +a.z*a.z);
}


inline float dot(const vec3&a, const vec3&b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline vec3 cross(const vec3&a, const vec3 &b){
	return vec3( a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline vec3 normalize(const vec3 &a){
	return a / length(a);
}

/*
	4x4xfloat matrix
*/
struct mat4 {
	float e[16];

	mat4(){
		memset(e,0x0,16*sizeof(float));
	}

	//Initializes matrix with x on main diagonal
	mat4(float x){
		memset(e,0x0,16*sizeof(float));
		for(int i=0;i<4;i++)
			e[i*4 + i] = x;
	}

	mat4(const float * arr){
		memcpy(e,arr,16*sizeof(float));
	}

	float operator [] (int i) const{ return e[i];}
	float & operator [] (int i){ return e[i];}

	//i row, j column
	//float operator () const (int i, int j){ return e[i*4 + j];}
	float & operator () (int i, int j){ return e[i*4 + j];}

	mat4 operator * (const mat4& right){
		mat4 m;
		for(int i=0; i<4;i++){
			for(int j=0; j<4;j++){
				const float *a = (float*)this + i*4 + j;
				const float *b = ((float *)&right) + j*4;
				float *c = (float*)&m + i*4;
				float *cmax = c + 4;
				while(c < cmax){
					*c++ += (*a) * (*b++);
				}
			}
		}
		return m;
	}

	vec4 operator * (const vec4 &vec){
		vec4 v;
		for(int i=0; i<4;i++){
			for(int j=0; j<4;j++){
				const float *a = (float*)this + i*4 + j;
				const float *b = ((float *)&vec) + j;
				float *c = (float*)&v + i;
				*c += (*a) * (*b);
			}
		}
		return v;
	}

#ifdef _DEBUG
	friend std::ostream & operator << (std::ostream& os, const mat4& m){
		for(int i=0;i<4;i++){
			for(int j=0;j<4;j++)
				os << m.e[i*4 + j] <<", ";
			os << std::endl;
		}
		return os;
	}
#endif
};

/*
	Transformation matrices
*/


mat4 translateMatrix(const vec3& v){
	mat4 m = mat4(1.0f);
	m(0,3) = v.x;
	m(1,3) = v.y;
	m(2,3) = v.z;
	return m;
}

mat4 scaleMatrix(const vec3& v){
	mat4 m;
	m(0,0) = v.x;
	m(1,1) = v.y;
	m(2,2) = v.z;
	m(3,3) = 1.0f;
	return m;
}


//angle in radians
mat4 rotateXMatrix(float angle){
	mat4 m = mat4(1.0f);
	const float c = cos(angle);
	const float s = sin(angle);
	m(1,1) = c;
	m(1,2) = -s;
	m(2,1) = s;
	m(2,2) = c;
	return m;
}

mat4 rotateYMatrix(float angle){
	mat4 m = mat4(1.0f);
	const float c = cos(angle);
	const float s = sin(angle);
	m(0,0) = c;
	m(0,2) = -s;
	m(2,0) = s;
	m(2,2) = c;
	return m;
}

mat4 rotateZMatrix(float angle){
	mat4 m = mat4(1.0f);
	const float c = cos(angle);
	const float s = sin(angle);
	m(0,0) = c;
	m(0,1) = -s;
	m(1,0) = s;
	m(1,1) = c;
	return m;
}


mat4 orthoMatrix( float left, float right, float bottom, float top, float near, float far )
{
	mat4 m;

	m(0,0) = 2.0f / (right - left);
	m(1,1) = 2.0f / (top - bottom);
	m(2,2) = -2.0f / (far - near);
	m(3,3) = 1.0f;

	m(0,3) = - (right + left) / (right - left);
	m(1,3) = - (top + bottom) / (top - bottom);
	m(2,3) = - (far + near) / (far - near);

	return m;
}


mat4 viewportMatrix(float x, float y, float width, float height){
	float vl = x;
	float vr = vl + width;
	float vb = y;
	float vt = y + height;

	mat4 m;
	m(0,0) = (vr - vl) * 0.5f;
	m(1,1) = (vt - vb) * 0.5f;
	m(2,2) = 0.5f;
	m(3,3) = 1.0f;

	m(0,3) = (vr + vl) * 0.5f;
	m(1,3) = (vt + vb) * 0.5f;
	m(2,3) = 0.5f;
	return m;
}


mat4 frustumMatrix(float l, float r, float b, float t,float n, float f){


	mat4 m;
	m(0,0) = (2 * n) / (r-l);
	m(1,1) = (2 * n) / (t-b);
	m(2,2) = -(f+n) / (f-n);
	m(3,3) = 0.0f;

	m(0,2) = (r+l) / (r-l);
	m(1,2) = (t+b) / (t-b);

	m(2,3) = (-2*f*n) / (f-n);
	m(3,2) = -1.0f;

	return m;
}

inline float degToRad(const float deg){
	return deg * (M_PI / 180.0f);
}

inline float radToDeg(const float rad){
	return rad * (180.0f / M_PI);
}


void transpose(const mat4 &m, mat4 & t){
	//Transpose!
	mat4 m_ = mat4(m);
	for(int i=0;i<4;i++){
		for(int k=0; k<4; k++){
			t(i,k) = m_(k,i);
		}
	}
}

bool invert(const mat4 &m, mat4 & out)
{
	float inv[16], det;
	int i;

	inv[0] = m[5]  * m[10] * m[15] -
		m[5]  * m[11] * m[14] -
		m[9]  * m[6]  * m[15] +
		m[9]  * m[7]  * m[14] +
		m[13] * m[6]  * m[11] -
		m[13] * m[7]  * m[10];

	inv[4] = -m[4]  * m[10] * m[15] +
		m[4]  * m[11] * m[14] +
		m[8]  * m[6]  * m[15] -
		m[8]  * m[7]  * m[14] -
		m[12] * m[6]  * m[11] +
		m[12] * m[7]  * m[10];

	inv[8] = m[4]  * m[9] * m[15] -
		m[4]  * m[11] * m[13] -
		m[8]  * m[5] * m[15] +
		m[8]  * m[7] * m[13] +
		m[12] * m[5] * m[11] -
		m[12] * m[7] * m[9];

	inv[12] = -m[4]  * m[9] * m[14] +
		m[4]  * m[10] * m[13] +
		m[8]  * m[5] * m[14] -
		m[8]  * m[6] * m[13] -
		m[12] * m[5] * m[10] +
		m[12] * m[6] * m[9];

	inv[1] = -m[1]  * m[10] * m[15] +
		m[1]  * m[11] * m[14] +
		m[9]  * m[2] * m[15] -
		m[9]  * m[3] * m[14] -
		m[13] * m[2] * m[11] +
		m[13] * m[3] * m[10];

	inv[5] = m[0]  * m[10] * m[15] -
		m[0]  * m[11] * m[14] -
		m[8]  * m[2] * m[15] +
		m[8]  * m[3] * m[14] +
		m[12] * m[2] * m[11] -
		m[12] * m[3] * m[10];

	inv[9] = -m[0]  * m[9] * m[15] +
		m[0]  * m[11] * m[13] +
		m[8]  * m[1] * m[15] -
		m[8]  * m[3] * m[13] -
		m[12] * m[1] * m[11] +
		m[12] * m[3] * m[9];

	inv[13] = m[0]  * m[9] * m[14] -
		m[0]  * m[10] * m[13] -
		m[8]  * m[1] * m[14] +
		m[8]  * m[2] * m[13] +
		m[12] * m[1] * m[10] -
		m[12] * m[2] * m[9];

	inv[2] = m[1]  * m[6] * m[15] -
		m[1]  * m[7] * m[14] -
		m[5]  * m[2] * m[15] +
		m[5]  * m[3] * m[14] +
		m[13] * m[2] * m[7] -
		m[13] * m[3] * m[6];

	inv[6] = -m[0]  * m[6] * m[15] +
		m[0]  * m[7] * m[14] +
		m[4]  * m[2] * m[15] -
		m[4]  * m[3] * m[14] -
		m[12] * m[2] * m[7] +
		m[12] * m[3] * m[6];

	inv[10] = m[0]  * m[5] * m[15] -
		m[0]  * m[7] * m[13] -
		m[4]  * m[1] * m[15] +
		m[4]  * m[3] * m[13] +
		m[12] * m[1] * m[7] -
		m[12] * m[3] * m[5];

	inv[14] = -m[0]  * m[5] * m[14] +
		m[0]  * m[6] * m[13] +
		m[4]  * m[1] * m[14] -
		m[4]  * m[2] * m[13] -
		m[12] * m[1] * m[6] +
		m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] +
		m[1] * m[7] * m[10] +
		m[5] * m[2] * m[11] -
		m[5] * m[3] * m[10] -
		m[9] * m[2] * m[7] +
		m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] -
		m[0] * m[7] * m[10] -
		m[4] * m[2] * m[11] +
		m[4] * m[3] * m[10] +
		m[8] * m[2] * m[7] -
		m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] +
		m[0] * m[7] * m[9] +
		m[4] * m[1] * m[11] -
		m[4] * m[3] * m[9] -
		m[8] * m[1] * m[7] +
		m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] -
		m[0] * m[6] * m[9] -
		m[4] * m[1] * m[10] +
		m[4] * m[2] * m[9] +
		m[8] * m[1] * m[6] -
		m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

	if (det == 0)
		return false;

	det = 1.0 / det;

	for (i = 0; i < 16; i++)
		out[i] = inv[i] * det;

	return true;
}

template<class T> T randReal() {
	// Return random float/double/etc. between 0 and 1 inclusive
	return static_cast<T>(rand()) / static_cast<T>(RAND_MAX);
}

#endif
