#ifndef SGL_CONTEXT
#define SGL_CONTEXT

#include <algorithm>
#include <utility>
#include <vector>
#include <stack>
#include <cstring>
#include <cmath>
#include <iostream>	 //DEBUG

#include "sgl.h"
#include "sgl_math.h"
#include "raytrace.h"
#include "liu.h"
#include "utils.h"

#include "buffer.h"

#define CHECK_BOUNDS if(x < 0 || x >= width || y < 0 || y >= height) return;

using namespace std;


class Context{

public:

	Buffer<vec3> buffer;
	Buffer<float> zbuffer;
	vector<vec3> vertexBuf;
	vec3 clearColor;
	vec3 color;
	sglEAreaMode areaMode;
	float pointSize;
	bool depthTest;

	bool beginCalled;
	bool beginSceneCalled;

	sglEElementType drawMode;

	PolyBuffer polybuffer;

	//Transforms
	stack<mat4> modelviewStack;
	stack<mat4> projectionStack;
	sglEMatrixMode matrixMode;

	bool PVMmatChanged;
	mat4 PVMmat;
	mat4 projMat;

	mat4 viewportMat;
	float radius_coeff;

	//Viewport
	vec4 viewport;
	float near;
	float far;

	Scene scene;

	mat4 inverseProjMat;
	mat4 inverseTransformMat;



	float cosTable[40];
	float sinTable[40];

	Context(const int width, const int height)
	  :buffer(width, height), polybuffer(width, height), zbuffer(width,height)
	{
		vertexBuf.reserve(32);
		clearColor = vec3(0,0,0);
		beginCalled = false;
		color = vec3(1,1,1);
		areaMode = SGL_FILL;
		pointSize = 1.0f;
		depthTest = false;
		modelviewStack.push(mat4(1.0f));
		projectionStack.push(mat4(1.0f));
		setViewport(0,0,width,height);
		PVMmatChanged = true;

		precomputeGoniometrics();
	}

	void setViewport(int x, int y, int width, int height){
		viewport = vec4(x,y,width,height);
		viewportMat = viewportMatrix(x,y,width,height);
	}

	inline void computePVMMatrix(){
		if(PVMmatChanged){
			PVMmat = projectionStack.top() * modelviewStack.top();
			PVMmatChanged = false;
		}
	}

	void precomputeGoniometrics(){
		float inc = 2*M_PI/40;
		int i=0;
		for(float t = 0; t < 2*M_PI; t += inc){
			cosTable[i] = cos(t);
			sinTable[i] = sin(t);
			i++;
		}

	}

	vec3 getRayDirection(float sx, float sy){
		//buf to ndc
		float x = (2.0f * sx) / buffer.width - 1.0f;
		float y = (2.0f * sy) / buffer.height - 1.0f;

		vec4 rdir = vec4(x,y,1.0f,1.0f);
		rdir = inverseProjMat * rdir;
		rdir.z = -1.0f;
		rdir.w = 0.0f;
		rdir = inverseTransformMat * rdir;

		vec3 final = vec3(rdir.x,rdir.y, rdir.z);

		//Return normalized ray direction
		return final * (1.0f / length(final));
	}

	inline void raytraceAAPartition(float sx, float sy, Ray& ray, vec3* color, unsigned level) {
		static const unsigned MAX_LEVEL = 16;   // max level of recursion (in powers of 2)

		if (level >= MAX_LEVEL) {
			// Reached max partition level, perfom one raytrace at the center
			ray.direction = getRayDirection(sx, sy);
			scene.raytrace(ray, color);
			return;
		}

		vec3 cc[4] = { *color, *color, *color, *color };
		float inc = 1.0 / (float)level;

		// Cast four rays at the corners of current partition:
		ray.direction = getRayDirection(sx - inc, sy - inc);
		scene.raytrace(ray, cc);
		ray.direction = getRayDirection(sx + inc, sy - inc);
		scene.raytrace(ray, cc+1);
		ray.direction = getRayDirection(sx - inc, sy + inc);
		scene.raytrace(ray, cc+2);
		ray.direction = getRayDirection(sx + inc, sy + inc);
		scene.raytrace(ray, cc+3);

		raytraceAAPartitionRecurse(cc, sx, sy, ray, color, level);
	}

	inline void raytraceAAPartitionRecurse(vec3 cc[4], float sx, float sy, Ray& ray, vec3* color, unsigned int level) {
		static const float THRE = 0.02;         // threshold of difference from avg

		float inc = 1.0 / (float)level;

		// Compute avg of the colors
		vec3 avg = (cc[0] + cc[1] + cc[2] + cc[3]) / 4.0;
		// See if any further partitions are needed:
		bool mod = false;
		inc /= 2;
		if ((avg - cc[0]).abs().max() > THRE) { mod = true; raytraceAAPartition(sx - inc, sy - inc, ray, cc, level * 2); }
		if ((avg - cc[1]).abs().max() > THRE) { mod = true; raytraceAAPartition(sx + inc, sy - inc, ray, cc+1, level * 2); }
		if ((avg - cc[2]).abs().max() > THRE) { mod = true; raytraceAAPartition(sx - inc, sy + inc, ray, cc+2, level * 2); }
		if ((avg - cc[3]).abs().max() > THRE) { mod = true; raytraceAAPartition(sx + inc, sy + inc, ray, cc+3, level * 2); }

		*color = mod ? (cc[0] + cc[1] + cc[2] + cc[3]) / 4.0 : avg;   // Recompute avg if any of the colors were modified
	}

	template<bool AA> void raytrace() {
		srand(time(NULL));

		//Calculate inverse matrices
		invert(projMat,inverseProjMat);
		invert(modelviewStack.top(),inverseTransformMat);

		//Get ray origin
		Ray ray;
		ray.origin = vec3(inverseTransformMat(0,3),inverseTransformMat(1,3),inverseTransformMat(2,3));

		if (!AA) {
			//Iterate through all pixels
			vec3 * ptr = buffer.data;
			for(int i=0; i < buffer.height; i++) {
				for(int k=0; k < buffer.width; k++) {
						ray.direction = getRayDirection(k,i);
						scene.raytrace(ray,ptr);
					ptr++;
				}
			}
		} else {
			// AA
			vec3 *ptr = buffer.data - 1;
			vec3 *aabuffer = new vec3[(buffer.width + 1) * (buffer.height + 1)];
			vec3 *aaptr = aabuffer - 1;
			{
				for(int i=0; i < buffer.height + 1; i++) {
					int k=0;
					for(; k < buffer.width; k++) {
						ptr++; aaptr++;
						ray.direction = getRayDirection((float)k - 0.5, (float)i - 0.5);
						*aaptr = *ptr;
						scene.raytrace(ray, aaptr);
					}
					aaptr++;
					ray.direction = getRayDirection((float)k + 0.5, (float)i + 0.5);
					*aaptr = *ptr;
					scene.raytrace(ray, aaptr);
					if (i == buffer.height - 1) ptr -= buffer.width;
				}
			}

			ptr = buffer.data;
			aaptr = aabuffer;
			vec3 *aaptr_nl = aabuffer + buffer.width + 1;
			for(int i=0; i < buffer.height; i++) {
				for(int k=0; k < buffer.width; k++) {
					vec3 cc[4] = { *aaptr, *(aaptr + 1), *aaptr_nl, *(aaptr_nl + 1) };
					raytraceAAPartitionRecurse(cc, k, i, ray, ptr, 4);
					ptr++;
					aaptr++;
					aaptr_nl++;
				}
				aaptr++;
				aaptr_nl++;
			}

			delete[] aabuffer;
		}

	}



	void begin(sglEElementType mode) {
		beginCalled = true;
		drawMode = mode;
		//Compute PVMMat only once
		if(!beginSceneCalled)
		computePVMMatrix();
	}

	void end() {

		if(beginSceneCalled && drawMode == SGL_POLYGON){
			scene.addTriangle(vertexBuf[0],vertexBuf[1],vertexBuf[2]);

		}
		else {
			switch (drawMode) {
				case SGL_LINES: drawLines(); break;
				case SGL_LINE_STRIP: drawLineStrip(); break;
				case SGL_LINE_LOOP: drawLineLoop(); break;
				case SGL_TRIANGLES: drawTriangles(); break;
				case SGL_POLYGON: drawPolygon(); break;
				default: break;
			}
		}
		vertexBuf.clear();

		beginCalled = false;
	}

	void addVertex(const vec4 &v){

		//Skip transforms when raycasting
		if(beginSceneCalled){
			vertexBuf.push_back(vec3(v.x,v.y,v.z));
			return;
		}

		vec3 pt = transform(v);

		if(drawMode == SGL_POINTS || areaMode == SGL_POINT){
			drawPoint(pt);
		}
		else {
			vertexBuf.push_back(pt);    // FIXME: vec3 - preserve Z
		}

	}
	void addVertex(vec3 v){ addVertex(vec4(v.x,v.y,v.z,1.0f)); }
	void addVertex(vec2 v){ addVertex(vec4(v.x,v.y,0.0f,1.0f)); }

	void drawPoint(vec3 screenCoord){
		int offset = float(pointSize)/2.0f;
		setBufferBlock(buffer,color,screenCoord.x - offset,screenCoord.y - offset,pointSize,pointSize);
	}

	void drawLines() {
		// FIXME: vertexBuf.size() %2 == 1 -> error (?)
		for (unsigned i = 0; i < vertexBuf.size(); i += 2) {
			bresenhamAbrash(buffer,vertexBuf[i],vertexBuf[i+1], color);
		}

	}

	void drawLineStrip(){
		for (unsigned i = 1; i < vertexBuf.size(); i++) {
			bresenhamAbrash(buffer,vertexBuf[i-1],vertexBuf[i], color);
		}
	}

	void drawCircle(float x,float y, float z, float radius){
		computePVMMatrix();
		vec3 pt = transform(vec4(x,y,z,1.0f));
		if(areaMode == SGL_POINT){
			drawPoint(pt);
			return;
		}
		mat4 conc = modelviewStack.top() * projectionStack.top() * viewportMat;
		float radius_coeff = sqrt(conc(0,0)*conc(1,1) - conc(1,0)*conc(1,0));

		if(areaMode == SGL_LINE)
			bresenhamCircle(buffer,ivec2(pt.x,pt.y),radius * radius_coeff,color);
		else
			bresenhamFillCircle(buffer,ivec2(pt.x,pt.y),radius * radius_coeff,color);
	}

	void drawEllipse(float x, float y, float z, float a, float b, float from = 0, float to = 2*M_PI, bool arc = false){
		computePVMMatrix();
		if(areaMode == SGL_POINT){
			drawPoint(transform(vec4(x,y,z,1.0f)));
			return;
		}

		vec4 center_wspace = vec4(x,y,z,1.0f);
		vec4 top_wspace = center_wspace;
		vec4 right_wspace = center_wspace;
		top_wspace.y += b;
		right_wspace.x += a;
		vec3 center = transform(center_wspace);
		vec3 top = transform(top_wspace);
		vec3 right= transform(right_wspace);
		const float at = dst_2(center,right);
		const float bt = dst_2(center,top);
		const float fi = (center.x-top.x) / length_2(center-top);
		const float sfi = sin(fi);
		const float cfi = cos(fi);
		vec3 pt;

		//vector<vec2> test;



		if(arc){
			float range = to - from;
			float angle = from;
			int n = 40*(range)/(2*M_PI);
			if(n%2) n--;
			float inc = (range)/n;
			for(int i=0; i < n+1; i++){
				pt.x = center.x + at * cos(angle)*cfi - bt * sin(angle)*sfi;
				pt.y = center.y + at * cos(angle)*sfi + bt * sin(angle)*cfi;
				angle += inc;
				vertexBuf.push_back(pt);
			}
		}
		else {

			for(int i=0; i< 40; i++){
				vec4 v  = vec4(x+ a*cosTable[i],y + b*sinTable[i],z,1.0f);
				vertexBuf.push_back(transform(v));
			}
			/*
			for(int i=0; i< 40; i++){
				pt.x = center.x + at * cosTable[i]*cfi - bt * sinTable[i]*sfi;
				pt.y = center.y + at * cosTable[i]*sfi + bt * sinTable[i]*cfi;
				vertexBuf.push_back(pt);

			}*/
		}

		if(areaMode == SGL_FILL){
			vector <vec3> triangleBuffer;
			for(int i=1;i<vertexBuf.size(); i++){
				triangleBuffer.push_back(vertexBuf[i-1]);
				triangleBuffer.push_back(vertexBuf[i]);
				triangleBuffer.push_back(center);
			}
			if(!arc){
				triangleBuffer.push_back(vertexBuf[0]);
				triangleBuffer.push_back(vertexBuf[vertexBuf.size()-1]);
				triangleBuffer.push_back(center);
			}
			vertexBuf.swap(triangleBuffer);
			drawPolygon();
		}
		else if(arc){
			drawLineStrip();
		}
		else {
			drawLineLoop();
		}


		/*float temp = pointSize;
		pointSize = 4;
		for(int i =0; i < test.size(); i++)
			drawPoint(test[i]);
		pointSize = temp;*/

		vertexBuf.clear();


	}

	void drawArc(float x, float y, float z, float radius, float from, float to){
		computePVMMatrix();

		if(areaMode == SGL_POINT){
			drawPoint(transform(vec4(x,y,z,1.0f)));
			return;
		}

		drawEllipse(x,y,z,radius,radius,from,to,true);
		return;

	}

	void drawLineLoop(){
		drawLineStrip();
		//Conect first and last
		vec3 last = vertexBuf[vertexBuf.size()-1];
		bresenhamAbrash(buffer,vertexBuf[0], last, color);
	}

	void drawTriangleOutline(const vec3& a, const vec3& b, const vec3& c, const vec3& color) {
		bresenhamAbrash(buffer, a, b, color);
		bresenhamAbrash(buffer, b, c, color);
		bresenhamAbrash(buffer, c, a, color);
	}

	void drawTriangleFilled(vec3& a, vec3& b, vec3& c, const vec3& color) {
		// TODO
//		drawTriangleOutline(a, b, c, color);

		// Sort by y:
		if (a.y > b.y) {
			if (b.y < c.y) swap(a, b);
			else swap(c, a);
		}
		else if (a.y > c.y) swap(c, a);
		if (c.y < b.y) swap(b, c);
	}

	void drawTriangles() {
		if (areaMode == SGL_LINE) {
			for (unsigned i = 2; i < vertexBuf.size(); i += 3) {
				drawTriangleOutline(vertexBuf[i-2], vertexBuf[i-1], vertexBuf[i], color);
			}
		}
		else if (areaMode == SGL_FILL) {
			for (unsigned i = 2; i < vertexBuf.size(); i += 3) {
				drawTriangleFilled(vertexBuf[i-2], vertexBuf[i-1], vertexBuf[i], color);
			}
		}
	}

	void drawPolygon() {
		if (vertexBuf.size() <= 2) return;

		if (areaMode != SGL_FILL) {
			drawLineLoop();
			return;
		}

		if (!depthTest) {
			polybuffer.tracePolygon<false>(vertexBuf);

			int ymin = clamp(polybuffer.ymin, 0, buffer.height - 1);
			vec3 *rowPtr = buffer.data + ymin*buffer.width;
			for (NET::const_iterator itn = polybuffer.net.begin(); itn != polybuffer.net.end(); ++itn) {
				const Nbucket& buck = *itn;
				for (size_t i = 1; i < buck.size(); i += 2) {
					int xL = buck[i-1].xL;
					int xR = buck[i].xR + 1;
					xL = clamp(xL, 0, buffer.width);
					xR = clamp(xR, 0, buffer.width);
					fill(rowPtr + xL, rowPtr + xR, color);
				}
				rowPtr += buffer.width;
			}
			polybuffer.reset();
		} else {

			fillPolygon(buffer,vertexBuf,color,(depthTest ? &zbuffer : NULL));
			return;

			///

			polybuffer.tracePolygon<true>(vertexBuf);

/*
			for (size_t i = 0; i < polybuffer.hpolygon.size(); i++) {
				const HSLS& hsls = polybuffer.hpolygon[i];

				float z = hsls.zL;
				float zinc = (hsls.zR - hsls.zL)/(hsls.xR - hsls.xL);
				for (int x = hsls.xL; x <= hsls.xR; x++) {
					float* zPtr = zbuffer.data + hsls.y*buffer.width + x;
					if (z < *zPtr) {
						*(buffer.data + hsls.y*buffer.width + x) = color;
						*zPtr = z;
					}
					z += zinc;
				}
			}
*/

			int ymin = clamp(polybuffer.ymin, 0, buffer.height - 1);
			vec3 *rowPtr = buffer.data + ymin*buffer.width;
			float *zPtr = zbuffer.data + ymin*buffer.width;
			for (NET::const_iterator itn = polybuffer.net.begin(); itn != polybuffer.net.end(); ++itn) {
				const Nbucket& buck = *itn;
				for (size_t i = 1; i < buck.size(); i += 2) {
					const Hel& a = buck[i-1];
					const Hel& b = buck[i];
					int xL = a.xL, xR = b.xR;
					float zL = a.zL, zR = b.zR;

					if ((xL >= buffer.width) || (xR < 0)) continue;
					if (xL < 0) {
						zL = (zR - zL) * (float)(-xL) / (float)(xR - xL);
						xL = 0;
					}
					if (xR >= buffer.width) {
						zR = (zR - zL) * (float)(buffer.width-xR) / (float)(xR - xL);
						xR = 0;
					}

					float z = zL;
					float zinc = (zR - zL)/(xR - xL);
					for (int x = xL; x <= xR; x++, z += zinc) {
						if (z < zPtr[x]) {
							rowPtr[x] = color;
							zPtr[x] = z;
						}
					}
				}

				rowPtr += buffer.width;
				zPtr += buffer.width;
			}
			polybuffer.reset();
		}
	}

	inline vec3 transform(const vec4 &v){
		//clip space < eyespace < model space
		vec4 pt = PVMmat * v;
		//perspective division
		if(pt.w!=0.0f){
			pt.x /= pt.w;
			pt.y /= pt.w;
			pt.z /= pt.w;
		}
		//viewport transformation
		//pt = viewportMat * pt;
		vec3 tv;
		tv.x = viewport.z * pt.x / 2 + viewport.x + viewport.z/2;
		tv.y = viewport.w * pt.y / 2 + viewport.y + viewport.w/2;
		tv.z = pt.z;
		//float near = 0;
		//float far = 1.0f;
		//tv.z = (far - near) * pt.z * 0.5f + (far+near)/2;
		return tv;
	}


	void beginScene(){
		beginSceneCalled = true;
		scene.clear();
		//TODO clear scene
	}

	void endScene(){
		beginSceneCalled = false;
	}

	stack<mat4> * getCurrentStack(){
		return ((matrixMode == SGL_MODELVIEW) ? &modelviewStack : &projectionStack);
	}

	inline mat4 & getCurrentMatrix(){
		return getCurrentStack()->top();
	}

	bool pushMatrix(){
		try{
			getCurrentStack()->push(getCurrentStack()->top());
		}catch(int e){
			return false;
		}
		return true;
	}

	bool popMatrix(){
		if(getCurrentStack()->size()==1)
			return false;
		getCurrentStack()->pop();
		PVMmatChanged = true;

		return true;
	}

	void loadIdentity(){
		getCurrentMatrix() = mat4(1.0f);
		PVMmatChanged = true;
	}

	void loadMatrix(const float *m){
		memcpy(&getCurrentMatrix(),m,sizeof(float)*16);
		PVMmatChanged = true;
	}

	void multMatrix(const float *m){

		//Transpose!
		mat4 t;
		mat4 m_ = mat4(m);
		for(int i=0;i<4;i++){
			for(int k=0; k<4; k++){
				t(i,k) = m_(k,i);
			}
		}

		multMatrix(t);
	}

	void multMatrix(const mat4& m){
		mat4 &top = getCurrentMatrix();
		top = top * m;
		PVMmatChanged = true;
	}

	void translate(float x, float y, float z){
		multMatrix(translateMatrix(vec3(x,y,z)));
	}

	void scale(float sx, float sy, float sz){
		multMatrix(scaleMatrix(vec3(sx,sy,sz)));
	}

	void rotate2D(float angle, float x, float y){
		multMatrix(
			translateMatrix(vec3(x,y,0.0f)) *
			rotateZMatrix(angle) *
			translateMatrix(vec3(-x,-y,0.0f))
		);
	}

	void rotateY(float angle){
		multMatrix(rotateYMatrix(angle));
	}

	void ortho(float l, float r, float b, float t, float n, float f){
		projMat = orthoMatrix(l, r, b, t, n, f);
		multMatrix(projMat);
	}

	void frustum(float l,float r, float b, float t, float n, float f){
		projMat = frustumMatrix(l,r,b,t,n,f);
		multMatrix(projMat);
	}

};



#endif
