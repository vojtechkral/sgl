#ifndef SGL_BUFFER
#define SGL_BUFFER

#include "sgl_math.h"
#include "utils.h"
#include <vector>
#include <list>
#include <map>

using namespace std;

template <typename T>
struct Buffer {
	T *data;

	int width;
	int height;

	Buffer(const int w, const int h) : width(w), height(h) {
		data = new T[width*height];
	}

	~Buffer() {
		delete[] data;
	}
};

template <typename T>
bool isInBounds(const Buffer<T> &b, int x, int y){
	return !(x < 0 || x >= b.width || y < 0 || y >= b.height);
}

template <typename T>
void clearBuffer(Buffer<T> &b, const T &clearVal){
	fill(b.data, b.data + b.width*b.height, clearVal);
}

template <typename T>
void setBufferBlock(Buffer<T> &b,const T &val, int x, int y, int rows, int cols){
	if(!isInBounds(b,x,y)) return;

	rows = clamp(rows, 0, b.width - x);
	cols = clamp(cols, 0, b.height - y);

	vec3 * pixelPtr = b.data + y*b.width + x;
	for(int i=0; i<cols; i++){
		fill(pixelPtr,pixelPtr + rows,val);
		pixelPtr+=b.width;
	}

}

void drawHorizontalRun(Buffer<vec3> &b, vec3 ** pixelPtr, int advance, int cnt, const vec3 color){
	vec3 * ptr = *pixelPtr;
	//Draw run
	for(int i=0; i < cnt; i++){
		*ptr = color;
		ptr += advance;
	}
	//next line
	ptr += b.width;
	*pixelPtr = ptr;
}

void drawVerticalRun(Buffer<vec3> &b, vec3 ** pixelPtr, int advance, int cnt, const vec3 color){
	vec3 * ptr = *pixelPtr;
	//Draw run
	for(int i=0; i < cnt; i++){
		*ptr = color;
		ptr += b.width;
	}

	//next column
	ptr += advance;
	*pixelPtr = ptr;
}

void bresenhamAbrash(Buffer<vec3> &buf, const vec3 &a, const vec3 &b, const vec3 color){

	int adjUp,adjDown,err,xAdvance,dx,dy;
	int wholeStep,initPixelCnt, finalPixelCnt, runLen;

	int startX, startY, endX,endY;

	//Draw top to bottom
	if(a.y > b.y){
		startX = b.x;
		startY = b.y;
		endX = a.x;
		endY = a.y;
	}
	else {
		startX = a.x;
		startY = a.y;
		endX = b.x;
		endY = b.y;
	}

	//Clamp line edges
	startX = clamp(startX,0,buf.width-1);
	startY = clamp(startY,0,buf.height-1);
	endX = clamp(endX,0,buf.width-1);
	endY = clamp(endY,0,buf.height-1);

	//Start of drawing
	vec3 * pixelPtr = buf.data + startY * buf.width + startX;

	//Deltas
	dx = endX - startX;
	dy = endY - startY;

	//Drawing left or right?
	if(dx < 0){
		xAdvance = -1;
		dx = -dx;
	}
	else {
		xAdvance = 1;
	}

	//Special case - vertical line
	if(dx == 0){
		for(int i=0;i<=dy;i++){
			*pixelPtr = color;
			pixelPtr += buf.width;
		}
		return;
	}
	//Special case - horizontal line
	if(dy == 0){
		for(int i=0;i<=dx;i++){
			*pixelPtr = color;
			pixelPtr += xAdvance;
		}
		return;
	}
	//Special case - diagonal line
	if(dy == dx){
		for(int i=0;i<=dx;i++){
			*pixelPtr = color;
			pixelPtr += xAdvance +buf.width;
		}
		return;
	}


	//if X is the major axis
	if(dx >= dy){
		//Minimum of pixels in a run
		wholeStep = dx / dy;

		//error adjustment when y steps by 1
		adjUp = (dx % dy) * 2;

		//error adjustment when error turns over
		adjDown = dy * 2;

		//initial error - initial 0.5 step along y
		err = (dx % dy) - dy*2;

		//initial and last run lenght - y advances only by 0.5
		initPixelCnt = (wholeStep/2) + 1;
		finalPixelCnt = initPixelCnt;

		//if run lenght is even, allocate one extra pixel to last run
		if(adjUp == 0 && (wholeStep & 0x01) == 0)
			initPixelCnt--;

		//odd number per run - add 0.5 to error term
		if((wholeStep & 0x01) != 0)
			err += dy;

		//Draw initial run
		drawHorizontalRun(buf,&pixelPtr,xAdvance,initPixelCnt,color);

		for(int i= 0; i < dy -1; i++){
			runLen = wholeStep;
			if((err += adjUp) > 0){
				runLen++;
				err -= adjDown;
			}
			drawHorizontalRun(buf,&pixelPtr,xAdvance,runLen,color);
		}
		drawHorizontalRun(buf,&pixelPtr,xAdvance,finalPixelCnt,color);
		return;
	}
	else {
		//Minimum of pixels in a run
		wholeStep = dy / dx;

		//error adjustment when y steps by 1
		adjUp = (dy % dx) * 2;

		//error adjustment when error turns over
		adjDown = dx * 2;

		//initial error - initial 0.5 step along y
		err = (dy % dx) - dx*2;

		//initial and last run lenght - y advances only by 0.5
		initPixelCnt = (wholeStep/2) + 1;
		finalPixelCnt = initPixelCnt;

		//if run lenght is even, allocate one extra pixel to last run
		if(adjUp == 0 && (wholeStep & 0x01) == 0)
			initPixelCnt--;

		//odd number per run - add 0.5 to error term
		if((wholeStep & 0x01) != 0)
			err += dx;

		//Draw initial run
		drawVerticalRun(buf,&pixelPtr,xAdvance,initPixelCnt,color);

		for(int i= 0; i < dx -1; i++){
			runLen = wholeStep;
			if((err += adjUp) > 0){
				runLen++;
				err -= adjDown;
			}
			drawVerticalRun(buf,&pixelPtr,xAdvance,runLen,color);
		}
		drawVerticalRun(buf,&pixelPtr,xAdvance,finalPixelCnt,color);
		return;

	}


}


//TODO boundary check
inline void setSymPixel(Buffer<vec3> &b, const vec3 &color, vec3 * centerPtr,int x, int y){
	int rowSpan = y*b.width;
	*(centerPtr + rowSpan + x) = color;
	*(centerPtr + rowSpan - x) = color;
	*(centerPtr - rowSpan + x) = color;
	*(centerPtr - rowSpan - x) = color;
	rowSpan = x*b.width;
	*(centerPtr + rowSpan + y) = color;
	*(centerPtr + rowSpan - y) = color;
	*(centerPtr - rowSpan + y) = color;
	*(centerPtr - rowSpan - y) = color;
}

inline void setSymRow(Buffer<vec3> &b,const vec3 &color, vec3 * centerptr, int x, int y){
	vec3 * line_start;
	vec3 * line_end;

	int rowSpan = y*b.width;
	line_start = centerptr + rowSpan - x;
	drawHorizontalRun(b,&line_start,1,2*x,color);
	line_start = centerptr - rowSpan - x;
	drawHorizontalRun(b,&line_start,1,2*x,color);
	rowSpan = x*b.width;
	line_start = centerptr + rowSpan - y;
	drawHorizontalRun(b,&line_start,1,2*y,color);
	line_start = centerptr - rowSpan - y;
	drawHorizontalRun(b,&line_start,1,2*y,color);
}

void bresenhamCircle(Buffer<vec3> &b,const ivec2 &s, const int r, const vec3 &color){
	int x = 0;
	int y = r;
	int p = 3 - 2*r;

	vec3 * centerPtr = b.data + s.y*b.width + s.x;

	while(x < y){
		setSymPixel(b,color,centerPtr,x,y);
		if(p < 0){
			p = p + 4*x + 6;
		}
		else {
			p = p + 4*(x-y) + 10;
			y--;
		}
		x++;
	}

	if(x==y) setSymPixel(b,color,centerPtr,x,y);
}

void bresenhamFillCircle(Buffer<vec3> &b,const ivec2 &s, const int r, const vec3 &color){
	int x = 0;
	int y = r;
	int p = 3 - 2*r;

	vec3 * centerPtr = b.data + s.y*b.width + s.x;

	while(x < y){
		setSymRow(b,color,centerPtr,x,y);
		if(p < 0){
			p = p + 4*x + 6;
		}
		else {
			p = p + 4*(x-y) + 10;
			y--;
		}
		x++;
	}

	if(x==y) setSymRow(b,color,centerPtr,x,y);
}


struct PolyItem {
	int ymax;
	float x;
	float minv;	
	float z;
	float zincr;
};

bool compare_by_x(const PolyItem& a, const PolyItem&  b){
	return (a.x < b.x);
}



void fillPolygon(Buffer<vec3> &buf, std::vector<vec3> &vertexBuffer, const vec3 &color, Buffer<float> * zbuffer = NULL){	
	map<int,list<PolyItem> > GET;
	list<PolyItem> AET;	
	static int id = 0;
	int scanline = 1<<16;

	//Contruct global edge table from edges
	int prev = vertexBuffer.size()-1;
	for(int i=0; i<vertexBuffer.size();i++){		
		const vec3 & A = vertexBuffer[prev];
		const vec3 & B = vertexBuffer[i];
		prev = i;		
		
		PolyItem e;
		int ymin;

		//Calculate delta
		float dx = A.x - B.x;
		float dy = A.y - B.y;
		//Delta z depends on direction
		float dz;

		//Assign values depending on y
		if(A.y > B.y){
			e.ymax = A.y;
			ymin = B.y;
			e.x = B.x;
			e.z = B.z;
			dz = A.z-B.z;
		}
		else {
			e.ymax = B.y;
			ymin = A.y;
			e.x = A.x;
			e.z = A.z;
			dz = B.z-A.z;
		}
		//Calculate increments of x and z
		e.minv = (dy==0) ? 0 : float(dx) / float(dy);			
		e.zincr = dz / abs(dy);
		
		//Add to global edge table
		GET[ymin].push_back(e);		

		//Get min y -> scanline to start on
		if(ymin < scanline) scanline = ymin;
		
	}
	

	//Point to first scanline
	vec3 * linePtr = buf.data + scanline*buf.width;	
	float * linePtrZ = NULL;
	if(zbuffer)
		linePtrZ = zbuffer->data + scanline*zbuffer->width;
	do{		
		//Move list from GET to end of AET
		AET.splice(AET.begin(),GET[scanline]);

		//Remove edges with ymax == scanline
		list<PolyItem>::iterator itAET = AET.begin();
		while(itAET!=AET.end()){
			if(itAET->ymax == scanline) itAET = AET.erase(itAET);
			else itAET++;
		}

		//End when there are not more edges
		if(AET.empty()) break;

		//Sort active edges
		AET.sort(compare_by_x);


		//Go through line and draw
		bool parity = false;
		itAET = AET.begin();
		itAET++;
		while(itAET!=AET.end()){	
			itAET--;
			list<PolyItem>::iterator a = itAET++;
			list<PolyItem>::iterator b = itAET;

			parity = !parity;			
			if(parity){		
				float dx = b->x - a->x;				
				//float interpInc = (dx == 0.0f) ? 0.0f : (b->z - a->z)/dx;
				if(dx == 0.0f){
					float lala = 5;
				}
				float interpInc = (b->z - a->z)/dx;
				int x = a->x;				
				float z = a->z;		
				int spanLength = ceil(dx);
				
				for(int i=0; i < spanLength; i++){											
					if(zbuffer){
						if(z < *(linePtrZ + x)){
							*(linePtr  + x) = color;
							*(linePtrZ + x) = z;
						}
					}
					else{					
						*(linePtr + x) = color;
					}
					z += interpInc;
					x++;
				}		
			}
			itAET++;
		}

		//Increment by x //TODO z interp
		itAET = AET.begin();
		while(itAET!=AET.end()){	
			itAET->x += itAET->minv;			
			itAET->z += itAET->zincr;
			itAET++;
		}


		//Move scanline
		linePtr+=buf.width;
		if(zbuffer) linePtrZ+=zbuffer->width;
		scanline++;				
	}while(true);



	

}


#endif
