//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2011/11/1
// Author: Jaroslav Krivanek, Jiri Bittner CTU Prague
//---------------------------------------------------------------------------



#include "sgl.h"
#include "context.h"
#include "utils.h"

//DEBUG
#include <iostream>
using namespace std;


#define CONTEXT_MAX_COUNT 32

#define CHECK_CONTEXT_BEFORE_BEGIN					\
	if(!currentContext || currentContext->beginCalled){	\
		setErrCode(SGL_INVALID_OPERATION);			\
		return;										\
	}

#define CHECK_CONTEXT_AFTER_BEGIN					\
	if(!currentContext || !currentContext->beginCalled){	\
		setErrCode(SGL_INVALID_OPERATION);			\
		return;										\
	}

#define CHECK_CONTEXT_BEFORE_BEGIN_SCENE					\
	if(!currentContext || currentContext->beginSceneCalled){	\
	setErrCode(SGL_INVALID_OPERATION);			\
	return;										\
	}

#define CHECK_CONTEXT_AFTER_BEGIN_SCENE					\
	if(!currentContext || !currentContext->beginSceneCalled){	\
	setErrCode(SGL_INVALID_OPERATION);			\
	return;										\
	}



//---------------------------------------------------------------------------
// Custom globals
//---------------------------------------------------------------------------

Context * contextArr[CONTEXT_MAX_COUNT];
int contextCount = 0;
int currentContextIndex = 0;
Context * currentContext = NULL; //Shorthand
Buffer<vec3> * currentBuffer = NULL; //Shorthand - avoid indirection as much as you can


//---------------------------------------------------------------------------
// Custom functions
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------
// Error code
//---------------------------------------------------------------------------

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

static inline void setErrCode(sglEErrorCode c)
{
  if(_libStatus==SGL_NO_ERROR)
    _libStatus = c;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError(void)
{
  sglEErrorCode ret = _libStatus;
  _libStatus = SGL_NO_ERROR;
  return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char* sglGetErrorString(sglEErrorCode error)
{
	static const char *errStrigTable[] =
	{
		"Operation succeeded",
		"Invalid argument(s) to a call",
		"Invalid enumeration argument(s) to a call",
		"Invalid call",
		"Quota of internal resources exceeded",
		"Internal library error",
		"Matrix stack overflow",
		"Matrix stack underflow",
		"Insufficient memory to finish the requested operation"
	};

	if(!inRange(error, SGL_NO_ERROR, SGL_OUT_OF_MEMORY)) {
		return "Invalid value passed to sglGetErrorString()";
	}

	return errStrigTable[error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit(void) {


#ifdef _DEBUG
	//Matrix / vector test
	cout << "Matrix multiplication test" << endl;
	float arrA[16] = {1, 5, 9, 8, 6, 8, 9, 1, 25, 8, 65, 4, 45, 8, 33, 6};
	mat4 a = mat4(arrA);
	float arrB[16] = {1,6,23,4, 6, 53, 2, 22, 2, 89, 6, 44, 25, 18, 3, 6};
	mat4 b = mat4(arrB);
	vec4 v = vec4(5,2,3,4);
	//cout << v;
	//cout << (a*v);

	//cout << translate(vec3(2.0f,3.0f,4.0f));
	//cout << scale(vec3(2.0f,3.0f,4.0f));
	//cout << rotateX(degToRad(45.0f)) << endl;
	//cout << rotateY(degToRad(45.0f)) << endl;
	//cout << rotateZ(degToRad(45.0f)) << endl;
#endif


}

void sglFinish(void) {

	//Cleanup contextes
	if(contextCount > 0){
		for(int i=0; i < 0; i++){
			if(contextArr[i])
				delete contextArr[i];
		}
	}
}

int sglCreateContext(int width, int height) {
	if(contextCount > CONTEXT_MAX_COUNT) {
		setErrCode(SGL_OUT_OF_RESOURCES);
		return -1;
	}

	int index = 0;
	//Find first free spot in array
	for(; index < CONTEXT_MAX_COUNT; index++){
		if(!contextArr[index]) break;
	}


	contextArr[index] = new (std::nothrow) Context(width, height);
	if(!contextArr){
		setErrCode(SGL_OUT_OF_MEMORY);
		return -1;
	}

	return (contextCount++);
}

void sglDestroyContext(int id) {
	if(id < 0 || id > contextCount){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	//Delete context
	delete contextArr[id];
	contextArr[id] = NULL;

	//Decrease number of contexts
	contextCount--;

	if(currentContextIndex == id){
		currentContext = NULL;
		currentBuffer = NULL;
	}

}



void sglSetContext(int id) {
	if(id < 0 || id > contextCount || !contextArr[id]){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	currentContextIndex = id;
	currentContext = contextArr[id];
	currentBuffer = &currentContext->buffer;
}


int sglGetContext(void) {
	if(!currentContext){
		setErrCode(SGL_INVALID_OPERATION);
		return -1;
	}
	return currentContextIndex;
}


float *sglGetColorBufferPointer(void) {
	if(!currentContext)
		return NULL;

	return reinterpret_cast<float*>(currentBuffer->data);
}


//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClear(unsigned what) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if(what & SGL_COLOR_BUFFER_BIT)
		clearBuffer(*currentBuffer,currentContext->clearColor);
	if(what & SGL_DEPTH_BUFFER_BIT)
		clearBuffer(currentContext->zbuffer,99999.0f);
	if(!(what & SGL_DEPTH_BUFFER_BIT || what & SGL_DEPTH_BUFFER_BIT))
		setErrCode(SGL_INVALID_VALUE);

}


void sglBegin(sglEElementType mode) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if (!inRange(mode, SGL_POINTS, SGL_AREA_LIGHT)) {
		setErrCode(SGL_INVALID_ENUM);
	}

	currentContext->begin(mode);
}

void sglEnd(void) {
	CHECK_CONTEXT_AFTER_BEGIN;

	currentContext->end();
}

void sglVertex4f(float x, float y, float z, float w) {
	currentContext->addVertex(vec4(x,y,z,w));
}

void sglVertex3f(float x, float y, float z) {
	currentContext->addVertex(vec3(x,y,z));
}

void sglVertex2f(float x, float y) {
	currentContext->addVertex(vec2(x,y));
}

void sglCircle(float x, float y, float z, float radius) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(radius < 0.0f){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	currentContext->drawCircle(x,y,z,radius);
}

void sglEllipse(float x, float y, float z, float a, float b) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(a < 0.0f || b < 0.0f){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	currentContext->drawEllipse(x,y,z,a,b);
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(radius < 0.0f){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	currentContext->drawArc(x,y,z,radius,from,to);
}


//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode( sglEMatrixMode mode ) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(mode == SGL_MODELVIEW || mode == SGL_PROJECTION)
		currentContext->matrixMode = mode;
	else {
		setErrCode(SGL_INVALID_ENUM);
	}
}

void sglPushMatrix(void) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(!currentContext->pushMatrix()){
		setErrCode(SGL_STACK_OVERFLOW);
	}
}

void sglPopMatrix(void) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(!currentContext->popMatrix()){
		setErrCode(SGL_STACK_UNDERFLOW);
	}
}

void sglLoadIdentity(void) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->loadIdentity();
}


void sglLoadMatrix(const float *matrix) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->loadMatrix(matrix);
}


void sglMultMatrix(const float *matrix) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->multMatrix(matrix);
}


void sglTranslate(float x, float y, float z) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->translate(x,y,z);
}


void sglScale(float scalex, float scaley, float scalez) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->scale(scalex,scaley,scalez);
}

void sglRotate2D(float angle, float centerx, float centery) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->rotate2D(angle,centerx,centery);
}

void sglRotateY(float angle) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->rotateY(angle);
}

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->ortho(left, right, bottom, top, near, far);
}

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(near < 0.0f || far < 0.0f){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	currentContext->frustum(left,right,bottom,top,near,far);
	//TODO
}


void sglViewport(int x, int y, int width, int height) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(width < 0 || height < 0){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	currentContext->setViewport(x,y,width,height);
}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------


void sglClearColor (float r, float g, float b, float /*alpha*/) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	currentContext->clearColor = vec3(r,g,b);  // alpha ignored as per spec

}



void sglColor3f(float r, float g, float b) {
	currentContext->color = vec3(r,g,b);
}

void sglAreaMode(sglEAreaMode mode) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if(mode == SGL_POINT || mode == SGL_LINE || mode== SGL_FILL)
		currentContext->areaMode = mode;
	else
		setErrCode(SGL_INVALID_ENUM);
}


void sglPointSize(float size) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if(size > 0.0f)
		currentContext->pointSize = size;
	else
		setErrCode(SGL_INVALID_VALUE);
}

void sglEnable(sglEEnableFlags cap) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(cap == SGL_DEPTH_TEST)
		currentContext->depthTest = true;
	else
		setErrCode(SGL_INVALID_ENUM);
}


void sglDisable(sglEEnableFlags cap) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(cap == SGL_DEPTH_TEST)
		currentContext->depthTest = false;
	else
		setErrCode(SGL_INVALID_ENUM);
}



//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {
	CHECK_CONTEXT_BEFORE_BEGIN_SCENE;
	currentContext->beginScene();
}

void sglEndScene() {
	CHECK_CONTEXT_AFTER_BEGIN_SCENE;
	currentContext->endScene();
}


void sglSphere(const float x,
			   const float y,
			   const float z,
			   const float radius) {
	CHECK_CONTEXT_AFTER_BEGIN_SCENE;
	currentContext->scene.addSphere(x,y,z,radius);
}

void sglMaterial(const float r,
				 const float g,
				 const float b,
				 const float kd,
				 const float ks,
				 const float shine,
				 const float T,
				 const float ior) {
	CHECK_CONTEXT_AFTER_BEGIN_SCENE
	currentContext->scene.addMaterial(r,g,b,kd,ks,shine,T,ior);
}

void sglPointLight(const float x,
				   const float y,
				   const float z,
				   const float r,
				   const float g,
				   const float b) {
	currentContext->scene.addLight(x,y,z,r,g,b);
}

void sglRayTraceScene() {
	currentContext->raytrace<true>();      // Template param: enable / disable adaptive AA
}

void sglRasterizeScene() {}

void sglEmissiveMaterial(
						 const float r,
						 const float g,
						 const float b,
						 const float c0,
						 const float c1,
						 const float c2
						 )
{
	CHECK_CONTEXT_AFTER_BEGIN_SCENE
	currentContext->scene.addEmmat(r, g, b, c0, c1, c2);
}

void sglEnvironmentMap(const int width,
					   const int height,
					   float *texels)
{
	CHECK_CONTEXT_AFTER_BEGIN_SCENE
	currentContext->scene.addEnvMap(width, height, texels);
}

