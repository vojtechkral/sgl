#ifndef LIU_H
#define LIU_H

#include <algorithm>
#include <vector>
#include <limits>

#include "sgl_math.h"
#include "utils.h"

using ::std::fill;
using ::std::sort;
using ::std::vector;
using ::std::numeric_limits;

// Structures & routines for Liu's polygon filling method


struct HSLS {
	int y, xL, xR;
	float zL, zR;

	HSLS(int y, int x, float z) :y(y), xL(x), xR(x), zL(z), zR(z) {}
	HSLS(const vec3& vec) :y(vec.y), xL(vec.x), xR(vec.x), zL(vec.z), zR(vec.z) {}
	HSLS(){}

	template<bool DEPTH> void merge(const HSLS& other) {
		if (DEPTH) {
			if (xL > other.xL) { xL = other.xL; zL = other.zL; }
			if (xR < other.xR) { xR = other.xR; zR = other.zR; }
		} else {
			if (xL > other.xL) xL = other.xL;
			if (xR < other.xR) xR = other.xR;
		}
	}

	template<bool DEPTH> void merge(int x, float z) {
		if (DEPTH) {
			if (x < xL) { xL = x; zL = z; }
			else if (xR < x) { xR = x; zR = z; }
		} else {
			if (x < xL) xL = x;
			else if (xR < x) xR = x; zR = z;
		}
	}
};

struct Hel {
	int xL, xR;
	float zL, zR;

	Hel(const Hel& other) :xL(other.xL), xR(other.xR), zL(other.zL), zR(other.zR) {}
	Hel(const HSLS& hsls) :xL(hsls.xL), xR(hsls.xR), zL(hsls.zL), zR(hsls.zR) {}

	bool operator<(const Hel& other) const {
		return xL < other.xL;
	}
};

typedef vector<HSLS> Hpolygon;
typedef vector<Hel> Nbucket;
typedef vector<Nbucket> NET;

struct PolyBuffer
{
	unsigned width, height;
	int ymin, ymax;
	Hpolygon hpolygon;
	NET net;

	PolyBuffer(unsigned width, unsigned height)
	  :width(width), height(height),
	   ymin(numeric_limits<int>::max()), ymax(numeric_limits<int>::min())
	{}

	void addPolyVertex(const vec3 &v) {
		if (v.y < ymin) ymin = v.y;
		if (v.y > ymax) ymax = v.y;
	}

	void reset() {
		ymin = numeric_limits<int>::max();
		ymax = numeric_limits<int>::min();
		hpolygon.clear();
		net.clear();
	}

	template<bool DEPTH> void bresenhamLiu(const vec3 a, const vec3 b) {
		int ax, ay, bx, by;
		ax = a.x; ay = a.y; bx = b.x; by = b.y;

		int dx =  abs(bx - ax);
		int sx = ax < bx ? 1 : -1;
		int dy = -abs(by - ay);
		int sy = ay < by ? 1 : -1;
		int err = dx+dy;
		int e2;

		float z, zincx, zincy;
		if (DEPTH) {
			z = a.z;
			zincx = (b.z - a.z) / fabs(a.x - b.x);
			zincy = (b.z - a.z) / fabs(a.y - b.y);
		}

		HSLS* hsls = &hpolygon[hpolygon.size() - 1];

		// Special case: horizontal
		if (dy == 0)  {
			hsls->merge<DEPTH>(ax, a.z);
			hsls->merge<DEPTH>(bx, b.z);
			return;
		}

		hsls->merge<DEPTH>(ax, z);
		while (ax != bx || ay != by) {
			e2 = 2*err;
			if (e2 >= dy) {   // e_xy+e_x > 0
				err += dy; ax += sx;
				if (DEPTH) { z += zincx; }
			}
			if (e2 <= dx) {   // e_xy+e_y < 0
				err += dx; ay += sy;
				if (DEPTH) { z += zincy; }
				// y advance
				hpolygon.push_back(HSLS(ay, ax, z));
				hsls = &hpolygon[hpolygon.size() - 1];
			} else {
				// x-only advance
				hsls->merge<DEPTH>(ax, z);
			}
		}
	}

	void insertHel(const HSLS& Py, const HSLS& Cy, const HSLS& Ny) {
		const Hel hel(Cy);
		const size_t y = Cy.y - ymin;

		// Liu's criterion:
		if ((Py.y > Cy.y && Ny.y < Cy.y) || (Py.y < Cy.y && Ny.y > Cy.y)) {
			net[y].push_back(hel);
		}
		else if ((Py.y > Cy.y && Ny.y > Cy.y) || (Py.y < Cy.y && Ny.y < Cy.y)) {
			net[y].push_back(hel);
			net[y].push_back(hel);
		}
	}

	template<bool DEPTH> void tracePolygon(const vector<vec3>& vertexBuf) {
		// Scan edges into hpolygon:
		addPolyVertex(vertexBuf[0]);
		hpolygon.push_back(HSLS(vertexBuf[0]));
		for (unsigned i = 1; i < vertexBuf.size(); i++) {
			addPolyVertex(vertexBuf[i]);
			bresenhamLiu<DEPTH>(vertexBuf[i-1], vertexBuf[i]);
		}
		size_t origsz = hpolygon.size();
		bresenhamLiu<DEPTH>(vertexBuf[vertexBuf.size()-1], vertexBuf[0]);
		if (hpolygon.size() > origsz) {
			hpolygon[0].merge<DEPTH>(hpolygon[hpolygon.size()-1]);
			hpolygon.pop_back();
		}

		// Generate NET:
		net.resize(ymax - ymin + 1);
		insertHel(hpolygon[hpolygon.size()-1], hpolygon[0], hpolygon[1]);
		for (unsigned i = 1; i < hpolygon.size() - 1; i++) {
			insertHel(hpolygon[i-1], hpolygon[i], hpolygon[i+1]);
		}
		insertHel(hpolygon[hpolygon.size()-2], hpolygon[hpolygon.size()-1], hpolygon[0]);

		// Sort NET:
		unsigned i = 0;
		for (NET::iterator it = net.begin(); it != net.end(); ++it) {
			sort(it->begin(), it->end());
		}
	}

};


#endif  //LIU_H
