#ifndef SGL_RAYTRACE
#define SGL_RAYTRACE

#include <vector>
#include <iostream>
#include <limits>
#include <cmath>
#include "sgl_math.h"
#include "buffer.h"
#include "utils.h"


using namespace std;


struct Ray {
	enum { RECURSION_CAP = 8 };

	vec3 origin;
	vec3 direction;
	unsigned recursion;

	Ray() :recursion(0) {}

	void bump(const vec3& normal, float amount) {
		origin = origin + (normal * amount);
	}

	bool mayRecurcse() const { return recursion < RECURSION_CAP; }
};

struct Material {
	float kd,ks,shine,T,ior;
	vec3 color;
	Material(const vec3 &color_, float kd_, float ks_, float shine_, float T_, float ior_):
		color(color_), kd(kd_), ks(ks_), shine(shine_), T(T_), ior(ior_) {}
	Material() {}

	vec3 precomputedDiffuse;
	vec3 precomputedSpecular;
};

struct Light {
	vec3 pos;
	vec3 color;
	Light(const vec3 &pos_, const vec3 &color_) : pos(pos_), color(color_) {}
	Light() {}
};

struct EmissiveMaterial {
	vec3 color;
	vec3 attenuation;
	EmissiveMaterial(const vec3& color, const vec3& attenuation) :color(color), attenuation(attenuation) {}

	float getAttenuation(float dist) const {
		return attenuation[0] + attenuation[1]*dist + attenuation[2]*dist*dist;
	}
};

class RenderableObject {
public:
	RenderableObject(Material* mat, EmissiveMaterial* emmat) :material(mat), emmat(emmat) {}
	virtual ~RenderableObject() {}

	Material *material;
	EmissiveMaterial *emmat;

	virtual bool intersect(const Ray & ray, float * t) = 0;
	virtual vec3 getNormal(const vec3 &point) const = 0;
};

class Sphere : public RenderableObject {
public:
	vec3 pos;
	float radius;
	float radius2;

	Sphere(const vec3 &p, const float r, Material * mat) :RenderableObject(mat, NULL) {
		pos = p;
		radius = r;
		radius2 = r*r;
	}

	virtual bool intersect(const Ray & ray, float * t) {
		vec3 centerToOrigin = ray.origin-pos;

		float rdiff = length(centerToOrigin) - radius;

//		float a = dot(ray.direction, ray.direction);
		// Since ray.direction is normalized, the dot prod is always 1.0, optimize away
		float b = 2*(dot(ray.direction, centerToOrigin));
		float c = dot(centerToOrigin, centerToOrigin) - radius2;
		float d = b*b - 4.0f*c;

		if(d < EPSILON) return false;

		if(d == 0.0f) {
			*t = (-b / 2.0f);
			return *t > 0.0f;
		}
		else {
			float sqrtD = sqrt(d);
			float t0 = (-b + sqrtD) / 2.0f;
			float t1 = (-b - sqrtD) / 2.0f;

			// sqrtD and a are both > 0  =>  t1 < t0

			if (t1 > EPSILON) *t = t1;
			else if (t0 > EPSILON) {
				float pk = EPSILON;
				*t = t0;
			}
			else return false;
		}
		return true;
	}

	virtual vec3 getNormal(const vec3 &point) const {
		return (point - pos) / radius;
	}
};

class Triangle : public RenderableObject {
public:
	vec3 v[3];
	vec3 normal;
	float area;

	Triangle(const vec3 &a, const vec3 &b, const vec3 &c, Material* mat, EmissiveMaterial* emmat)
	  :RenderableObject(mat, emmat) {
		v[0] = a;
		v[1] = b;
		v[2] = c;
		normal = normalize(cross(a-b,b-c));
		area = emmat ? length(cross(b-a, c-a))/2.0f : 0.0f;
	}

	virtual bool intersect(const Ray & ray, float * t) {
		bool singleSided = true;

		vec3 e1 = v[1] - v[0];
		vec3 e2 = v[2] - v[0];

		vec3 pvec = cross(ray.direction, e2);
		float det = dot(e1, pvec);

		if (det < EPSILON) return false;
		float invDet = 1 / det;

		vec3 tvec = ray.origin - v[0];
		//Compute u
		float u = dot(tvec, pvec) * invDet;
		if (u < 0 || u > 1)
			return false;

		vec3 qvec = cross(tvec, e1);
		//Compute v
		float v = dot(ray.direction, qvec) * invDet;
		if (v < 0 || v > 1 || u + v > 1)
			return false;

		// calculate t, ray intersects triangle
		*t = dot(e2, qvec) * invDet;
		return true;
	}

	virtual vec3 getNormal(const vec3 &point) const {
		return normal;
	}

	float emissiveArea() const {
		return area;
	}

};





class Scene {
public:
	vector<RenderableObject*> objects;
	//vector<Light> lights;
	Light light;
	vector<Material*> materials;
	vector<EmissiveMaterial*> emmats;
	vector<RenderableObject*> emmat_objs;

	Material *currentMaterial;
	EmissiveMaterial *currentEmmat;

	int em_width, em_height;
	float *envmap;

	Scene() :currentMaterial(NULL), currentEmmat(NULL), envmap(NULL) {}

	void clear() {
		for(int i=0; i < objects.size(); i++)   delete objects[i];
		objects.clear();
		currentMaterial = NULL;

		for(int i=0; i < materials.size(); i++) delete materials[i];
		materials.clear();
		currentEmmat = NULL;

		for(int i=0; i < materials.size(); i++) delete emmats[i];
		emmats.clear();
		emmat_objs.clear();

		envmap = NULL;
	}

	void addSphere(const float x, const float y, const float z, const float radius) {
		objects.push_back(new Sphere(vec3(x,y,z), radius, currentMaterial));
	}

	void addTriangle(const vec3 &a, const vec3 &b, const vec3 &c) {
		Triangle *t = new Triangle(a,b,c, currentMaterial, currentEmmat);
		objects.push_back(t);
		if (currentEmmat) emmat_objs.push_back(t);
	}

	void addMaterial(const float r,
		const float g,
		const float b,
		const float kd,
		const float ks,
		const float shine,
		const float T,
		const float ior) {
		currentMaterial = new Material(vec3(r,g,b),kd,ks,shine,T,ior);
		materials.push_back(currentMaterial);
	}

	void addLight(const float x,
		const float y,
		const float z,
		const float r,
		const float g,
		const float b) {
			light = Light(vec3(x,y,z),vec3(r,g,b));
			for(int i=0; i<materials.size();i++) {
				materials[i]->precomputedDiffuse = light.color * materials[i]->kd * materials[i]->color;
				materials[i]->precomputedSpecular = light.color * materials[i]->ks;
			}
	}

	void addEmmat(
		const float r,
		const float g,
		const float b,
		const float c0,
		const float c1,
		const float c2) {
		const vec3 color(r, g, b);
		if (color != vec3(0, 0, 0)) {
			currentEmmat = new EmissiveMaterial(color, vec3(c0, c1, c2));
			emmats.push_back(currentEmmat);
		} else {
			currentEmmat = NULL;
		}
	}

	void addEnvMap(const int width,
								 const int height,
								 float *texels) {
		em_width = width;
		em_height = height;
		envmap = texels;
	}

	bool refractionDir(const vec3& orig_dir, vec3& dir, vec3& norm, float ior) {
		float gamma, sqrterm;

		float d = dot(orig_dir, norm);

		if (d < 0.0) {
			// from outside into the inside of object
			gamma = 1.0 / ior;
		}
		else {
			// from the inside to outside of object
			gamma = ior;
			d = -d;
			norm = norm * -1;
		}
		sqrterm = 1.0 - gamma * gamma * (1.0 - d * d);

		// Check for total internal reflection, do nothing if it applies.
		if (sqrterm > 0.0) {
			sqrterm = d * gamma + sqrt(sqrterm);
			dir = norm * -sqrterm + orig_dir * gamma;
			dir.cap(1.0f);
			return true;
		}
		return false;
	}

	vec3 phong(const RenderableObject* obj, const Ray &ray, const vec3& N, const vec3& isec_p, const vec3& light_p, const vec3& light_c) {
		vec3 L = normalize(light_p - isec_p);
		float c = dot(L,N);

		const Material * mat = obj->material;

		// Diffuse part
		vec3 color = light_c * mat->kd * mat->color * dot(L, N);

		// Specular part
		if(c > 0.0f) {
			vec3 E = normalize(ray.origin - isec_p);
			vec3 R = normalize(N * 2*c - L);
			float b = dot(E, R);
			if(b > 0.0f)
				color = color + light_c * mat->ks * pow(b, mat->shine);
		}

		return color;
	}

	long intersect(const Ray& ray, float& t, float maxT = numeric_limits<float>::max(), bool any = false) {
		// Returns index of nearest object (or random object if any == true) or -1 if no intersection is found within [EPSILON, maxT]
		long ret = -1;
		t = numeric_limits<float>::max();

		for(size_t i = 0; i < objects.size(); i++) {
			float objT;
			bool objIntr = objects[i]->intersect(ray, &objT);
			if (objIntr && inRange(objT, EPSILON, ::min(t, maxT))) {
				if (any) return objIntr;
				ret = i;
				t = objT;
			}
		}

		return ret;
	}

	bool lightShadowed(const vec3& isec_p, const vec3& N, const vec3& light_p) {
		Ray ray_shd;
		ray_shd.direction = normalize(light_p - isec_p);
		ray_shd.origin = isec_p;
		ray_shd.bump(N, 1e-2f);
		float shd_maxT = length(light_p - ray_shd.origin) - 3e-2f;
		float shd_t;
		return intersect(ray_shd, shd_t, shd_maxT, true) >= 0;
	}

	void emmatLight(const Triangle* t, const RenderableObject* obj, const Ray& ray, vec3& add_to, const vec3& N, const vec3& isec_p) {
		enum { SAMPLES = 16 };

		const EmissiveMaterial *emmat = t->emmat;

		// Generate samples
		static vec3 samples[SAMPLES];
		const vec3& a = t->v[0];
		const vec3 u = t->v[1] - a;
		const vec3 v = t->v[2] - a;
		for (unsigned i = 0; i < SAMPLES; i++) {
			float rnd = sqrt(randReal<float>());
			float alpha = 1 - rnd;
			float beta  = randReal<float>()*rnd;
			samples[i] = a + u*alpha + v*beta;
		}

		vec3 sum;
		for (size_t i = 0; i < SAMPLES; i++) {
			const vec3& xL = samples[i];

			if (lightShadowed(isec_p, N, xL)) continue;

			const vec3 L = normalize(xL - isec_p);
			float attn = -dot(L, t->getNormal(xL)) / emmat->getAttenuation(length(xL - isec_p));
			sum = sum + phong(obj, ray, N, isec_p, xL, vec3(1, 1, 1)) * attn;
		}

		const vec3 intensity = emmat->color * t->emissiveArea() / SAMPLES;
		add_to = add_to + sum * intensity;
	}

	void castReflect(const Ray& ray, vec3& add_to, const vec3& N, const vec3& isec_p, const Material& mat) {
		vec3 reflect = vec3(0,0,0);
		Ray ray_rfl;
		ray_rfl.origin = isec_p;
		ray_rfl.direction = normalize(ray.direction - N * (2.0f * dot(ray.direction, N)));
		ray_rfl.bump(N, 1e-2f);
		ray_rfl.recursion = ray.recursion + 1;

		raytrace(ray_rfl, &reflect);
		add_to = add_to + reflect * mat.ks;
	}

	void castRefract(const Ray& ray, vec3& add_to, const vec3& N, const vec3& isec_p, const Material& mat) {
		vec3 refract = vec3(0,0,0);
		vec3 refr_norm = N;
		Ray ray_rfr;
		ray_rfr.origin = isec_p;

		if (refractionDir(ray.direction, ray_rfr.direction, refr_norm, mat.ior)) {
			ray_rfr.bump(refr_norm * -1.0f, 1e-2f);
			ray_rfr.recursion = ray.recursion + 1;
			raytrace(ray_rfr, &refract);
			add_to = add_to + refract * mat.T;
		}
	}

	void envMapColor(const Ray& ray, vec3* color) {
		if (!envmap) return;

		float d = length_2(ray.direction);
		float r = d > 0.0f ? 0.159154943*acos(ray.direction.z)/d : 0.0f;
		float u = 0.5f + ray.direction.x * r;
		float v = 0.5f + ray.direction.y * r;
		int iu = u * em_width;
		int iv = em_height - v * em_height;

		const float* texel = envmap + iv*em_width*3 + iu*3;
		color->x = texel[0];
		color->y = texel[1];
		color->z = texel[2];
	}

	void raytrace(const Ray& ray, vec3* color) {
		float t;
		long intersect_idx = intersect(ray, t);

		if (intersect_idx > -1)
		{
			const RenderableObject *obj = objects[intersect_idx];
			const vec3 isec_p = ray.origin + ray.direction * t;

			if (obj->emmat)
			{
				*color = obj->emmat->color;   // Ideally, attenuation would be applied
				color->cap(1.0f);
				return;
			}

			vec3 sum;
			const vec3 N = obj->getNormal(isec_p);

			// Point light
			if (!lightShadowed(isec_p, N, light.pos)) {
				sum = sum + phong(obj, ray, N, isec_p, light.pos, light.color);
			}

			// Emissive material lighting
			for (size_t i = 0; i < emmat_objs.size(); i++) {
				emmatLight(static_cast<const Triangle*>(emmat_objs[i]), obj, ray, sum, N, isec_p);
			}

			// Reflection
			if (obj->material->ks > 0.0f && ray.mayRecurcse()) {
				castReflect(ray, sum, N, isec_p, *obj->material);
			}

			// Refraction
			if (obj->material->T > 0.0f && ray.mayRecurcse()) {
				castRefract(ray, sum, N, isec_p, *obj->material);
			}

			sum.cap(1.0f);

			*color = sum;
		}
		else {
			envMapColor(ray, color);
		}
	}

	~Scene(){
		clear();
	}
};

#endif
