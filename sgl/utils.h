#ifndef UTILS_H
#define UTILS_H


template<class T> const T max(const T a, const T b) { return a > b ? a : b; }
template<class T> const T min(const T a, const T b) { return a < b ? a : b; }

template<class T> const bool inRange(T x, T a, T b) { return x >= a && x <= b; }
template<class T> const bool inRangeSharp(T x, T a, T b) { return x > a && x < b; }

template<class T> const T clamp(T x, T a, T b) { return x < a ? a : x > b ? b : x; }

template<class T> const T sgn(T x) { return (T(0) < x) - (x < T(0)); }

template<class TX, class TF> const bool flag(TX x, TF flag) { return !!(x & flag); }
template<class TX, class TF> void setFlag(TX& x, TF flag, bool set) { set ? x |= flag : x &= ~flag; }

template<class T> void mswap(T& a, T& b) { T tmp = a; a = b; b = tmp; }

#endif  //UTILS_H
