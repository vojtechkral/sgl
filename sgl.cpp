//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2011/11/1
// Author: Jaroslav Krivanek, Jiri Bittner CTU Prague
//---------------------------------------------------------------------------



#include "sgl.h"
#include "context.h"

//DEBUG
#include <iostream>	
using namespace std;
	

#define CONTEXT_MAX_COUNT 32

#define CHECK_CONTEXT_BEFORE_BEGIN					\
	if(!currentContext || currentContext->begin){	\
		setErrCode(SGL_INVALID_OPERATION);			\
		return;										\
	}

#define CHECK_CONTEXT_AFTER_BEGIN					\
	if(!currentContext || !currentContext->begin){	\
		setErrCode(SGL_INVALID_OPERATION);			\
		return;										\
	}



//---------------------------------------------------------------------------
// Custom globals
//---------------------------------------------------------------------------

Context * contextArr[CONTEXT_MAX_COUNT];
int contextCount = 0;
int currentContextIndex = 0;
Context * currentContext = NULL; //Shorthand
Buffer * currentBuffer = NULL; //Shorthand - avoid indirection as much as you can


//---------------------------------------------------------------------------
// Custom functions
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------
// Error code
//---------------------------------------------------------------------------

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

static inline void setErrCode(sglEErrorCode c) 
{
  if(_libStatus==SGL_NO_ERROR)
    _libStatus = c;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError(void) 
{
  sglEErrorCode ret = _libStatus;
  _libStatus = SGL_NO_ERROR;
  return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char* sglGetErrorString(sglEErrorCode error)
{
  static const char *errStrigTable[] = 
  {
      "Operation succeeded",
      "Invalid argument(s) to a call",
      "Invalid enumeration argument(s) to a call",
      "Invalid call",
      "Quota of internal resources exceeded",
      "Internal library error",
      "Matrix stack overflow",
      "Matrix stack underflow",
      "Insufficient memory to finish the requested operation"
  };

  if((int)error<(int)SGL_NO_ERROR || (int)error>(int)SGL_OUT_OF_MEMORY ) {
    return "Invalid value passed to sglGetErrorString()"; 
  }

  return errStrigTable[(int)error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit(void) {}

void sglFinish(void) {

	//Cleanup contextes
	if(contextCount > 0){
		for(int i=0; i < 0; i++){
			if(contextArr[i]) 
				delete contextArr[i];							
		}
	}
}

int sglCreateContext(int width, int height) {
	if(contextCount > CONTEXT_MAX_COUNT) {
		setErrCode(SGL_OUT_OF_RESOURCES);
		return -1;
	}
	
	int index = 0;
	//Find first free spot in array
	for(; index < CONTEXT_MAX_COUNT; index++){	
		if(!contextArr[index]) break;
	}

	//TODO catch out of memory exception and set err to SGL_OUT_OF_MEMORY
	contextArr[index] = new Context(width, height);

	return (contextCount++);
}

void sglDestroyContext(int id) {	
	if(id < 0 || id > contextCount){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	//Delete context
	delete contextArr[id];
	contextArr[id] = NULL;

	//Decrease number of contexts
	contextCount--;

	if(currentContextIndex == id){		
		currentContext = NULL;
		currentBuffer = NULL;
	}

}



void sglSetContext(int id) {
	if(id < 0 || id > contextCount || !contextArr[id]){
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	currentContextIndex = id;
	currentContext = contextArr[id];
	currentBuffer = currentContext->buffer;
}


int sglGetContext(void) {
	if(!currentContext){
		setErrCode(SGL_INVALID_OPERATION);
		return -1;
	}
	return currentContextIndex;
}


float *sglGetColorBufferPointer(void) {
	if(!currentContext)	
		return NULL;

	return currentBuffer->data;
}


//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClearColor (float r, float g, float b, float alpha) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	currentContext->clearColor = vec4(r,g,b,alpha);
}

void sglClear(unsigned what) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if(SGL_COLOR_BUFFER_BIT)
		currentBuffer->clear(currentContext->clearColor);	
	else if(SGL_DEPTH_BUFFER_BIT)
		{} //TODO	
	else
		setErrCode(SGL_INVALID_VALUE);	
	
}


void sglBegin(sglEElementType mode) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	//TODO invalid enum

	currentContext->begin = true;
	

}

void sglEnd(void) {
	CHECK_CONTEXT_AFTER_BEGIN;

	currentContext->begin = false;

}

void sglVertex4f(float x, float y, float z, float w) {}

void sglVertex3f(float x, float y, float z) {}

void sglVertex2f(float x, float y) {}

void sglCircle(float x, float y, float z, float radius) {}

void sglEllipse(float x, float y, float z, float a, float b) {}

void sglArc(float x, float y, float z, float radius, float from, float to) {}

//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode( sglEMatrixMode mode ) {}

void sglPushMatrix(void) {}

void sglPopMatrix(void) {}

void sglLoadIdentity(void) {}

void sglLoadMatrix(const float *matrix) {}

void sglMultMatrix(const float *matrix) {}

void sglTranslate(float x, float y, float z) {}

void sglScale(float scalex, float scaley, float scalez) {}

void sglRotate2D(float angle, float centerx, float centery) {}

void sglRotateY(float angle) {}

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {}

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {}

void sglViewport(int x, int y, int width, int height) {}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
	currentContext->color = vec3(r,g,b);
}

void sglAreaMode(sglEAreaMode mode) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	
	if(mode & (SGL_POINT | SGL_LINE | SGL_FILL))
		currentContext->areaMode = mode;
	else
		setErrCode(SGL_INVALID_ENUM);

	
}


void sglPointSize(float size) {
	CHECK_CONTEXT_BEFORE_BEGIN;

	if(size > 0.0f)
		currentContext->pointSize = size;
	else
		setErrCode(SGL_INVALID_VALUE);
}


void sglEnable(sglEEnableFlags cap) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(cap == SGL_DEPTH_TEST)
		currentContext->depthTest = true;
	else
		setErrCode(SGL_INVALID_ENUM);
}


void sglDisable(sglEEnableFlags cap) {
	CHECK_CONTEXT_BEFORE_BEGIN;
	if(cap == SGL_DEPTH_TEST)
		currentContext->depthTest = false;
	else
		setErrCode(SGL_INVALID_ENUM);
}



//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {}

void sglEndScene() {}

void sglSphere(const float x,
			   const float y,
			   const float z,
			   const float radius) {}

void sglMaterial(const float r,
				 const float g,
				 const float b,
				 const float kd,
				 const float ks,
				 const float shine,
				 const float T,
				 const float ior) {}

void sglPointLight(const float x,
				   const float y,
				   const float z,
				   const float r,
				   const float g,
				   const float b) {}

void sglRayTraceScene() {}

void sglRasterizeScene() {}

void sglEnvironmentMap(const int width,
					   const int height,
					   float *texels)
{}

void sglEmissiveMaterial(
						 const float r,
						 const float g,
						 const float b,
						 const float c0,
						 const float c1,
						 const float c2
						 )
{}

